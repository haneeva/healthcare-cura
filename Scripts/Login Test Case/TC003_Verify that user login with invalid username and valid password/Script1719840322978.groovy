import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Open Browser

WebUI.openBrowser('')

//Navigate to Cura healthcare website

WebUI.navigateToUrl(GlobalVariable.URL)

println("Navigated to Cura healthcare website")

//Click Hamburger Menu

WebUI.click(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/a_CURA Healthcare_menu-toggle'))

println ("Hamburger Menu is clickable")

//Click Login Menu

WebUI.click(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/a_Login'))

println ("Login menu is clickable")

//Enter Invalid Username

WebUI.setText(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/input_Username_username'), username)

println("Username is filled")

//Enter Valid Password

WebUI.setEncryptedText(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/input_Password_password'), password)

println("Password is filled")

//Click Login button

WebUI.click(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/button_Login'))

println ("Login button is clickable")

//Verify the error message displays

String ActualErrorMessage = WebUI.getText(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/p_Login failed Please ensure the username a_eb55b5'))

String ExpectedErrorMessage = "Login failed! Please ensure the username and password are valid."

WebUI.verifyMatch(ActualErrorMessage, ExpectedErrorMessage, false)

println("An error message should be displayed")

//Take a screenshot for evidence

WebUI.takeScreenshot("C:\\Cura Screenshot\\ErrorMessage.png")