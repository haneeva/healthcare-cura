@regression @login
Feature: As user, I want to be able to access the Cura healthcare website, so I can use the feature

  Background:
  	Given User navigate to cura website
  
  Scenario Outline: [Positive Test] Verify that user login with valid credential
  	When User click hamburger menu
  	And User click login menu
  	Then User should be directed to login page
  	And User enter username <username>
  	And User enter password <password>
  	And User click login button
  	Then User login successfully
  	And User click hamburger menu after login
  	And User click logout menu
  	Then User logout from their account
  	
  Examples:
  |username|password												|
  |John Doe|g3/DOGG74jC3Flrr3yH+3D/yKbOqqUNM|


	Scenario Outline: [Negative Test] Verify that user login with valid username and invalid password
		When User click hamburger menu
  	And User click login menu
  	Then User should be directed to login page
  	And User enter username <username>
  	And User enter password <password>
  	And User click login button
  	Then User see the error message
  	
   Examples:
  |username|password								|
  |John Doe|R2GnBZXYIh+Ge2cjcK3J5g==|
  
	Scenario Outline: [Negative Test] Verify that user login with invalid username and valid password
		When User click hamburger menu
  	And User click login menu
  	Then User should be directed to login page
  	And User enter username <username>
  	And User enter password <password>
  	And User click login button
  	Then User see the error message
  	
  Examples:
  |username|password												|
  |John Ala|g3/DOGG74jC3Flrr3yH+3D/yKbOqqUNM|
  

	Scenario Outline: [Negative Test] Verify that user login with invalid username and password
		When User click hamburger menu
  	And User click login menu
  	Then User should be directed to login page
  	And User enter username <username>
  	And User enter password <password>
  	And User click login button
  	Then User see the error message
  	
  Examples:
  |username|password								|
  |John Ala|R2GnBZXYIh+Ge2cjcK3J5g==|
  
  
	Scenario: [Negative Test] Verify that user login with empty data
		When User click hamburger menu
  	And User click login menu
  	Then User should be directed to login page
  	And User click login button
  	Then User see the error message