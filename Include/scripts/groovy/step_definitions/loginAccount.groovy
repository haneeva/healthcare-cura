package step_definitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class loginAccount {

	//Login with Valid Credentials

	@Given("User navigate to cura website")
	def navigateToUrl() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl(GlobalVariable.URL)
		println ("Open browser and access website")
	}

	@When("User click hamburger menu")
	def clickHamburgerMenu() {
		WebUI.click(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/a_CURA Healthcare_menu-toggle'))
		println ("Hamburger Menu is clickable")
	}

	@And("User click login menu")
	def clickLoginMenu() {
		WebUI.click(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/a_Login'))
		println ("Login menu is clickable")
	}

	@Then("User should be directed to login page")
	def loginPageDisplayed() {
		//Verify the login page by URL
		String ActualPage = WebUI.getUrl()
		String ExpectedPage = "https://katalon-demo-cura.herokuapp.com/profile.php#login"
		WebUI.verifyMatch(ActualPage, ExpectedPage, false)

		//Take a screenshot for evidence
		WebUI.takeScreenshot("C:\\Cura Screenshot\\loginpage.png")

		println("Login Page is displayed")
	}

	@And("User enter username (.*)")
	def enterUsername(String username) {
		WebUI.setText(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/input_Username_username'), username)
		println("Username is filled")
	}

	@And("User enter password (.*)")
	def enterPassword(String password) {
		WebUI.setEncryptedText(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/input_Password_password'), password)
		println("Password is filled")
	}

	@And("User click login button")
	def clickLoginButton() {
		WebUI.click(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/button_Login'))
		println ("Login button is clickable")
	}

	@Then("User login successfully")
	def loginSuccessfully() {
		//Verify the appointment page after login by URL
		String ActualAppointmentPage = WebUI.getUrl()
		String ExpectedAppointmentPage = "https://katalon-demo-cura.herokuapp.com/#appointment"
		WebUI.verifyMatch(ActualAppointmentPage, ExpectedAppointmentPage, false)

		//Take a screenshot for evidence
		WebUI.takeScreenshot("C:\\Cura Screenshot\\MakeAppointmentpage.png")

		println("Login is successful")
	}

	//Logout from their account

	@And("User click hamburger menu after login")
	def clickToogleLogin() {
		WebUI.click(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/i_CURA Healthcare_fa fa-bars'))
		println ("Hamburger Toogle button is clickable")
	}

	@And("User click logout menu")
	def clickLogoutMenu() {
		WebUI.click(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/a_Logout'))
		println ("Logout menu is clickable")
	}

	@Then("User logout from their account")
	def logoutSuccessfully() {
		//Verify the page after logout
		String ActualLogoutPage = WebUI.getUrl()
		String ExpectedLogoutPage = "https://katalon-demo-cura.herokuapp.com/"
		WebUI.verifyMatch(ActualLogoutPage, ExpectedLogoutPage, false)

		//Take a screenshot for evidence
		WebUI.takeScreenshot("C:\\Cura Screenshot\\LogoutPage.png")

		println("Logout is successful")
	}

	//Error Message for Login with INvalid Credentials
	@Then("User see the error message")
	def errorMessage() {
		//Verify the error message displays
		String ActualErrorMessage = WebUI.getText(findTestObject('Object Repository/Login/Page_CURA Healthcare Service/p_Login failed Please ensure the username a_eb55b5'))
		String ExpectedErrorMessage = "Login failed! Please ensure the username and password are valid."
		WebUI.verifyMatch(ActualErrorMessage, ExpectedErrorMessage, false)

		//Take a screenshot for evidence
		WebUI.takeScreenshot("C:\\Cura Screenshot\\ErrorMessage.png")

		println("An error message should be displayed")
	}
}