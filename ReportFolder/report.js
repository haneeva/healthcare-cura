$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/loginAccount.feature");
formatter.feature({
  "name": "As user, I want to be able to access the Cura healthcare website, so I can use the feature",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@regression"
    },
    {
      "name": "@login"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Verify that user login with valid credential",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@positive"
    }
  ]
});
formatter.step({
  "name": "User click hamburger menu",
  "keyword": "When "
});
formatter.step({
  "name": "User click login menu",
  "keyword": "And "
});
formatter.step({
  "name": "User should be directed to login page",
  "keyword": "Then "
});
formatter.step({
  "name": "User enter username \u003cusername\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User enter password \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User click login button",
  "keyword": "And "
});
formatter.step({
  "name": "User login successfully",
  "keyword": "Then "
});
formatter.step({
  "name": "User click hamburger menu after login",
  "keyword": "And "
});
formatter.step({
  "name": "User click logout menu",
  "keyword": "And "
});
formatter.step({
  "name": "User logout from their account",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "John Doe",
        "g3/DOGG74jC3Flrr3yH+3D/yKbOqqUNM"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "User navigate to cura website",
  "keyword": "Given "
});
formatter.match({
  "location": "loginAccount.navigateToUrl()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify that user login with valid credential",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@regression"
    },
    {
      "name": "@login"
    },
    {
      "name": "@positive"
    }
  ]
});
formatter.step({
  "name": "User click hamburger menu",
  "keyword": "When "
});
formatter.match({
  "location": "loginAccount.clickHamburgerMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login menu",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User should be directed to login page",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.loginPageDisplayed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter username John Doe",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.enterUsername(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter password g3/DOGG74jC3Flrr3yH+3D/yKbOqqUNM",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.enterPassword(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login button",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User login successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.loginSuccessfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click hamburger menu after login",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickToogleLogin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click logout menu",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLogoutMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logout from their account",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.logoutSuccessfully()"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Verify that user login with valid username and invalid password",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "User click hamburger menu",
  "keyword": "When "
});
formatter.step({
  "name": "User click login menu",
  "keyword": "And "
});
formatter.step({
  "name": "User should be directed to login page",
  "keyword": "Then "
});
formatter.step({
  "name": "User enter username \u003cusername\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User enter password \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User click login button",
  "keyword": "And "
});
formatter.step({
  "name": "User see the error message",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "John Doe",
        "R2GnBZXYIh+Ge2cjcK3J5g\u003d\u003d"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "User navigate to cura website",
  "keyword": "Given "
});
formatter.match({
  "location": "loginAccount.navigateToUrl()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify that user login with valid username and invalid password",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@regression"
    },
    {
      "name": "@login"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "User click hamburger menu",
  "keyword": "When "
});
formatter.match({
  "location": "loginAccount.clickHamburgerMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login menu",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User should be directed to login page",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.loginPageDisplayed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter username John Doe",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.enterUsername(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter password R2GnBZXYIh+Ge2cjcK3J5g\u003d\u003d",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.enterPassword(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login button",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see the error message",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.errorMessage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Verify that user login with invalid username and valid password",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "User click hamburger menu",
  "keyword": "When "
});
formatter.step({
  "name": "User click login menu",
  "keyword": "And "
});
formatter.step({
  "name": "User should be directed to login page",
  "keyword": "Then "
});
formatter.step({
  "name": "User enter username \u003cusername\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User enter password \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User click login button",
  "keyword": "And "
});
formatter.step({
  "name": "User see the error message",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "John Ala",
        "g3/DOGG74jC3Flrr3yH+3D/yKbOqqUNM"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "User navigate to cura website",
  "keyword": "Given "
});
formatter.match({
  "location": "loginAccount.navigateToUrl()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify that user login with invalid username and valid password",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@regression"
    },
    {
      "name": "@login"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "User click hamburger menu",
  "keyword": "When "
});
formatter.match({
  "location": "loginAccount.clickHamburgerMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login menu",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User should be directed to login page",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.loginPageDisplayed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter username John Ala",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.enterUsername(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter password g3/DOGG74jC3Flrr3yH+3D/yKbOqqUNM",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.enterPassword(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login button",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see the error message",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.errorMessage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Verify that user login with invalid username and valid password",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "User click hamburger menu",
  "keyword": "When "
});
formatter.step({
  "name": "User click login menu",
  "keyword": "And "
});
formatter.step({
  "name": "User should be directed to login page",
  "keyword": "Then "
});
formatter.step({
  "name": "User enter username \u003cusername\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User enter password \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User click login button",
  "keyword": "And "
});
formatter.step({
  "name": "User see the error message",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "John Ala",
        "R2GnBZXYIh+Ge2cjcK3J5g\u003d\u003d"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "User navigate to cura website",
  "keyword": "Given "
});
formatter.match({
  "location": "loginAccount.navigateToUrl()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify that user login with invalid username and valid password",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@regression"
    },
    {
      "name": "@login"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "User click hamburger menu",
  "keyword": "When "
});
formatter.match({
  "location": "loginAccount.clickHamburgerMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login menu",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User should be directed to login page",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.loginPageDisplayed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter username John Ala",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.enterUsername(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter password R2GnBZXYIh+Ge2cjcK3J5g\u003d\u003d",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.enterPassword(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login button",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see the error message",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.errorMessage()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "User navigate to cura website",
  "keyword": "Given "
});
formatter.match({
  "location": "loginAccount.navigateToUrl()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify that user login with empty data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@regression"
    },
    {
      "name": "@login"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "User click hamburger menu",
  "keyword": "When "
});
formatter.match({
  "location": "loginAccount.clickHamburgerMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login menu",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginMenu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User should be directed to login page",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.loginPageDisplayed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click login button",
  "keyword": "And "
});
formatter.match({
  "location": "loginAccount.clickLoginButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see the error message",
  "keyword": "Then "
});
formatter.match({
  "location": "loginAccount.errorMessage()"
});
formatter.result({
  "status": "passed"
});
});